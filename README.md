This repository contains Ansible tasks to copy files to, and execute commands
on a remote host that has BusyBox but no Python.
